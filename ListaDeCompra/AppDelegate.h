//
//  AppDelegate.h
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 10/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

