//
//  main.m
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 10/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
