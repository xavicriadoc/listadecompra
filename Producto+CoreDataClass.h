//
//  Producto+CoreDataClass.h
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 11/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Producto : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Producto+CoreDataProperties.h"
