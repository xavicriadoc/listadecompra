//
//  Producto+CoreDataProperties.h
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 11/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//
//

#import "Producto+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Producto (CoreDataProperties)

+ (NSFetchRequest<Producto *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *nombre;

@end

NS_ASSUME_NONNULL_END
