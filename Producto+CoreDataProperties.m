//
//  Producto+CoreDataProperties.m
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 11/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//
//

#import "Producto+CoreDataProperties.h"

@implementation Producto (CoreDataProperties)

+ (NSFetchRequest<Producto *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Producto"];
}

@dynamic nombre;

@end
