//
//  AddProductViewController.h
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 10/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Producto+CoreDataProperties.h"

@interface AddProductViewController : UIViewController
@property (strong, nonatomic) Producto *selectedProducto;
@property (strong, nonatomic) IBOutlet UITextField *newprod;
- (IBAction)savePressed:(UIBarButtonItem *)sender;
- (IBAction)cancelPressed:(UIBarButtonItem *)sender;

@end
