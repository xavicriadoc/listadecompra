//
//  AddProductViewController.m
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 10/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

#import "AddProductViewController.h"
#import "AppDelegate.h"
#import "Producto+CoreDataClass.h"


@interface AddProductViewController ()

@end

@implementation AddProductViewController{
    Producto *producto;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.selectedProducto) {
        self.newprod.text = self.selectedProducto.nombre;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)savePressed:(UIBarButtonItem *)sender {
    AppDelegate *delegado = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *contexto = delegado.persistentContainer.viewContext;
    
    if (self.selectedProducto) {
        producto = self.selectedProducto;
    }else{
    producto = (Producto*)[NSEntityDescription insertNewObjectForEntityForName:@"Producto" inManagedObjectContext:contexto];
    }
    
    producto.nombre = self.newprod.text;
    
    NSError *error = nil;
    if (![contexto save:&error]) {
        NSLog(@"Ha habido un error %@", [error localizedDescription]);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
