//
//  ProductesMenuTableTableViewController.m
//  ListaDeCompra
//
//  Created by Xavier Criado Carmona on 10/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

#import "ProductesMenuTableTableViewController.h"
#import "AppDelegate.h"
#import "Producto+CoreDataClass.h"
#import "AddProductViewController.h"

@interface ProductesMenuTableTableViewController ()
@end

@implementation ProductesMenuTableTableViewController{
    NSFetchedResultsController *fetchResultController;
    NSArray *productos;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Producto"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"nombre" ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    
    AppDelegate *delegado = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *contexto = delegado.persistentContainer.viewContext;
    
    if (contexto != nil) {
        
        fetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:contexto sectionNameKeyPath:nil cacheName:nil];
        fetchResultController.delegate = self;
        
        NSError *error;
        if ([fetchResultController performFetch:&error]) {
            productos = fetchResultController.fetchedObjects;
        } else{
            NSLog(@"No se han podido recuperar productos, error: %@", [error localizedDescription]);
        }
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate


#pragma mark - Table view data source
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return productos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Producto *producto = (Producto*)productos[indexPath.row];
    cell.textLabel.text = producto.nombre;
    
    return cell;
}

#pragma mark - NSFetchResultsControllerDelegate

-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView beginUpdates];
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            [self.tableView reloadData];
            break;
    }
    productos = controller.fetchedObjects;
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}

#pragma mark - UITableViewDataSource
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppDelegate *delegado = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *contexto = delegado.persistentContainer.viewContext;
    
    if (contexto != nil) {
        
        Producto *productToDelete = (Producto*)[fetchResultController objectAtIndexPath:indexPath];
        [contexto deleteObject:productToDelete];
        
        NSError *error = nil;
        if (![contexto save:&error]) {
            NSLog(@"No se puede borrar el objeto %@", [error localizedDescription]);
        }
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat altura = self.view.frame.size.height;
    // NSLog(@"La altura es %f", altura);
    float numeroCeldas = 12.35;
    return altura/numeroCeldas;
    
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"updateProduct"]) {
        Producto *product = productos[self.tableView.indexPathForSelectedRow.row];
        UINavigationController *destVC = segue.destinationViewController;
        AddProductViewController *vc = (AddProductViewController*)destVC.topViewController;
        vc.selectedProducto = product;
    }
}

@end
